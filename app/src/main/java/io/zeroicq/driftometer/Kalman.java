package io.zeroicq.driftometer;

public class Kalman {
    private double X0;//predicted state
    private double P0;//predicted covariance

    private double F; //factor

    private double Q; //measure noise
    private double H; //factor
    private double R; //environment noise

    public double getState() {
        return State;
    }

    private double State;
    private double Covariance;

    public Kalman(double q, double r) {
        F = 1;
        H = 1;
        R = r;
        Q = q;

    }

    public void setState(double state, double covariance) {
        State = state;
        Covariance = covariance;
    }

    public void Correct(double data) {
        //time update - prediction
        X0 = F*State;
        P0 = F*Covariance*F + Q;

        //measurement update - correction
        double K = H*P0/(H*P0*H + R);
        if (Double.isNaN(K))
            K = 0;
        State = X0 + K*(data - H*X0);
        Covariance = (1 - K*H)*P0;
    }
}
