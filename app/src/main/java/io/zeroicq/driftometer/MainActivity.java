package io.zeroicq.driftometer;

import android.content.Context;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private TextView mAzimuthText;

    private SensorManager mSensorManager;
    private LocationManager mLocationManager;

    private Sensor mMagnetFieldSensor;
    private Sensor mAccelerometerSensor;

    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private double mAzimuth;//in radians

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //components
        mAzimuthText = (TextView) findViewById(R.id.azimuthText);

        //managers
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //sensors
        mMagnetFieldSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        // Register a listener for the sensor.
        super.onResume();
        mSensorManager.registerListener(mMagnetFieldEventListener, mMagnetFieldSensor, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(mAccelerometerEvenListener, mAccelerometerSensor, SensorManager.SENSOR_DELAY_FASTEST);

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mMagnetFieldEventListener);
        mSensorManager.unregisterListener(mAccelerometerEvenListener);
        mLocationManager.removeUpdates(mLocationListener);
    }

    final double mSmoothCompasThreshold = 0.1;//1'
    final double mSmoothCoefficient = 0.99;

    private void updateAzimuth() {
        float[] orientation = new float[3];
        float[] rMat = new float[9];

        SensorManager.getRotationMatrix(rMat, null, mAccelerometerReading, mMagnetometerReading);
        SensorManager.getOrientation(rMat, orientation);

//
        double currAngle = orientation[0];
        double delta = Math.abs(currAngle - mAzimuth);

        if (delta <= mSmoothCompasThreshold) {
            double lastSin = mSmoothCoefficient * Math.sin(mAzimuth) + (1-mSmoothCoefficient) * Math.sin(currAngle);
            double lastCos = mSmoothCoefficient * Math.cos(mAzimuth) + (1-mSmoothCoefficient) * Math.cos(currAngle);

            mAzimuth = Math.atan2(lastSin, lastCos);

        } else {
            mAzimuth = currAngle;
        }
//
//        if (delta >=1)
//            ((TextView) findViewById(R.id.textView2)).setText(Float.toString(delta));

        ((TextView)findViewById(R.id.textBearing)).setText(String.format(Locale.getDefault(), "%.5f", delta));
//        ((TextView)findViewById(R.id.textView2)).setText(String.format(Locale.getDefault(), "%.5f", Math.toDegrees(delta)));

        mAzimuthText.setText(String.format(Locale.getDefault(), "%.4f", mAzimuth));
        ((TextView)findViewById(R.id.textView)).setText(String.format(Locale.getDefault(), "%d", Math.round(Math.toDegrees(mAzimuth))));
    }

    private SensorEventListener mMagnetFieldEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            System.arraycopy(event.values, 0, mMagnetometerReading, 0, mMagnetometerReading.length);
            updateAzimuth();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {}
    };

    private SensorEventListener mAccelerometerEvenListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            System.arraycopy(event.values, 0, mAccelerometerReading, 0, mAccelerometerReading.length);
            updateAzimuth();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {}
    };

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            location.getBearing();

            GeomagneticField gField = new GeomagneticField((float)location.getLatitude(),
                    (float)location.getLongitude(), (float)location.getAltitude(), System.currentTimeMillis());
//            mGeoDeclination = gField.getDeclination();
            ((TextView) findViewById(R.id.textBearing)).setText(Float.toString(location.getBearing()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            ((TextView) findViewById(R.id.textView)).setText(provider + " " + Integer.toString(status));
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


}
